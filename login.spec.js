const {test , expect} = require('@playwright/test');
test('login',async({page}) =>{
    await page.goto('https://testing.kriyadocs.com/')
    await page.locator('xpath=/html/body/div[2]/div/form/div/div[2]/div[1]/div/input').fill('duvajaakarthikeyan@outlook.com');
    await page.locator('xpath=/html/body/div[2]/div/form/div/div[2]/div[2]/div/input').fill('Happy_Authors');
    await page.locator('xpath=/html/body/div[2]/div/form/div/div[2]/div[5]/div[1]/a').click();
     //next line is to ensure that it has been logged in
     await expect(page).toHaveTitle('Kriyadocs | Login')
})